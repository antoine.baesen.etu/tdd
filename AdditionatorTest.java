package PasDefault;
import PasDefault.FooBarFix;

import static org.junit.Assert.assertEquals;

import org.junit.*;

public class AdditionatorTest {
	
	@Test
	public void voidTest() throws Exception
	{
		assertEquals("0",Additionator.add(""));
	}
	
	@Test
	public void separatorTest() throws Exception
	{
		assertEquals("13",Additionator.firstValor("13,24"));
		assertEquals("24",Additionator.secondValor("13,24"));
	}
	
	@Test
	public void addIntTest() throws Exception
	{
		assertEquals("37.0",Additionator.add("13.0,24.0"));
	}
	
	@Test
	public void addDoubleTest() throws Exception
	{
		assertEquals("3.7",Additionator.add("1.3,2.4"));
	}
	
	@Test(expected=Exception.class)
	public void TestEndVirg() throws Exception
	{
		Additionator.add("1.3,2.4,");
	}
	
	@Test
	public void retourTest() throws Exception
	{
		assertEquals("3.7",Additionator.add("1.3\n2.4"));
	}
	
	@Test(expected=Exception.class)
	public void TestDoubleSeparator() throws Exception
	{
		Additionator.add("1.3\n,2.4,");
	}
	
	@Test(expected=Exception.class)
	public void TestNegatif() throws Exception
	{
		Additionator.add("1.3,-2.4,");
	}
	
	@Test
	public void newSeparators() throws Exception
	{
		assertEquals("MOUTON",Additionator.findSeparator("//MOUTON\n3.0MOUTON1.0"));
	}
	
	/*
	@Test
	public void newSeparatorsAdd() throws Exception
	{
		assertEquals("4.0",Additionator.add("//MOUTON\n3.0MOUTON1.0"));
	}*/
}
