package PasDefault;

import java.util.List;
import java.util.ArrayList;

public class Additionator {
	List<String> entries = new ArrayList<String>();
	
	public static String add(String entry) throws Exception
	{
		if(entry == "")
		{
			return "0";
		}
		String res = "";
		res = "" + (StringToDouble(firstValor(entry)) + StringToDouble(secondValor(entry)));
		return res;
	}

	public static String firstValor(String string) throws Exception{
		String res = "";
		for(int i = 0; i < string.length() ; ++i)
		{
			if(string.charAt(i) == ',' || string.charAt(i) == '\n')
			{
				return res;
			}
			else
			{
				res += string.charAt(i);
			}
		}
		
		throw new Exception("Pas de séparateur de valeurs");
	}

	public static String secondValor(String string) throws Exception {
		String res = "";
		boolean secondState = false;
		for(int i = 0; i < string.length() ; ++i)
		{
			if(string.charAt(i) == '-')
			{
				throw new Exception("nombre negatifs interdits");
			}
			if(string.charAt(i) == ',' || string.charAt(i) == '\n')
			{
				if(!secondState)
				{
					secondState = true;
				}
				else
				{
					throw new Exception("Separateur de trop");
				}
			}
			else
			{
				if(secondState)
					{
						res += string.charAt(i);
					}
			}
		}
		
		if(!secondState)
		{
			throw new Exception("Pas de séparateur de valeurs");
		}
			
		return res;
	}
	
	public static Double StringToDouble(String s) throws Exception
	{
		try
		{
			return Double.valueOf(s);
		}
		catch(Exception e)
		{
			System.out.println("Not number entry");
		}
		throw new Exception("Une des entree n'est pas un nombre");
	}

	public static String findSeparator(String string) throws Exception{
		String res = "";
		int state = 0;
		for(int i = 0 ; i < string.length() ; ++i)
		{
			if(string.charAt(i) == '\n' && state == 0)
			{
				state = 1;
			}
			else if(state == 0)
			{
				res += string.charAt(i);
			}
		}
		if(res.substring(0, 2).equals("//"))
		{
			return res.substring(2);
		}
		else
		{
			throw new Exception("Erreur de separateur");
		}
	}

}
