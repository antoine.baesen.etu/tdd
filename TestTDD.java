package PasDefault;
import PasDefault.FooBarFix;

import static org.junit.Assert.assertEquals;

import org.junit.*;

public class TestTDD {
	
	
	@Test
	public void voidTest()
	{
		assertEquals("1",FooBarFix.FBF(1));
	}
	
	@Test
	public void simpleNumberTest()
	{
		assertEquals("2",FooBarFix.FBF(2));
	}
	
	
	@Test
	public void FooTest()
	{
		assertEquals("Foo",FooBarFix.FBF(6));
	}
	
	@Test
	public void FooFooTest()
	{
		assertEquals(FooBarFix.FBF(3),"FooFoo");
		assertEquals(FooBarFix.FBF(13),"Foo");
		assertEquals(FooBarFix.FBF(33),"FooFooFoo");
	}
	
	@Test
	public void ContainBarTest()
	{
		assertEquals("Bar",FooBarFix.FBF(52));
	}
	
	@Test
	public void DivisibleBarTest()
	{
		assertEquals("Bar",FooBarFix.FBF(2*5));
	}
	
	@Test
	public void BarBarTest()
	{
		assertEquals("BarBar",FooBarFix.FBF(5*5));
	}
	
	@Test
	public void FooBarTest()
	{
		assertEquals("FooBarBar",FooBarFix.FBF(3*5));
	}
	
	@Test
	public void ContainQixTest()
	{
		assertEquals("Qix",FooBarFix.FBF(71));
	}
	
	@Test
	public void DivisibleQixTest()
	{
		assertEquals("Qix",FooBarFix.FBF(2*7));
	}
	
	@Test
	public void QixTest()
	{
		assertEquals("QixQixQix",FooBarFix.FBF(7*11));
	}
	
	@Test
	public void FooBarQixTest()
	{
		assertEquals("FooBarQixBar",FooBarFix.FBF(7*5*3));
	}
	
}
